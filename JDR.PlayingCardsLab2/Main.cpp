#include <iostream>
#include <conio.h>
#include<string>
using namespace std;
enum Rank
{
	TWO=2,THREE,FOUR,FIVE,SIX,SEVEN,EIGHT,NINE,TEN,JACK,QUEEN,KING,ACE
};
enum Suit 
{
	HEARTS=1,SPADES,DIAMONDS,CLUBS
};
struct Card
{
	Suit suit;
	Rank rank;
};
void PrintCard(Card card);

int main() 
{
	Card card1;
	card1.suit = HEARTS;
	card1.rank = TWO;

	int i;
	cout << "Enter a rank:";
	cin >> i;
	

	card1.rank = (Rank)i;

	Card card2;
	card2.rank = FOUR;

	PrintCard(card1);

	_getch();
	return 0;
}

void PrintCard(Card card)
{
	cout << "The ";
	switch (card.rank)
	{
	case 2: cout << "Two";  break;
	case 3: cout << "Three"; break;
	case 4: cout << "Four"; break;
	case 5: cout << "Five"; break;
	case 6: cout << "Six"; break;
	case 7: cout << "Seven"; break;
	case 8: cout << "Eight"; break;
	case 9: cout << "Nine"; break;
	case 10:cout << "Ten";  break;
	case 11:cout << "Jack";  break;
	case 12:cout << "Queen";  break;
	case 13:cout << "King";  break;
	case 14:cout << "Ace";  break;
	}
	
	cout << " of ";

	switch (card.suit) 
	{
	case 1: cout << "Hearts"; break;
	case 2: cout << "Spades"; break;
	case 3: cout << "Diamonds"; break;
	case 4: cout << "Clubs"; break;
	}
}

Card HighCard(Card card1, Card card2) 
{
	if (card1.rank == card2.rank) 
	{
		cout << "Cards are the same value!";
	}
	else if (card1.rank > card2.rank)
	{
		cout << card1.rank << " is greater than " << card2.rank;
	}
	else if (card2.rank>card1.rank)
	{
		cout << card2.rank << " is greater than " << card2.rank;
	}
}

